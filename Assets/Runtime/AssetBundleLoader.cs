﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

public class AssetBundleLoader
{
    public static event Action<string, float, float> OnProgress;
    public static string baseUrl = "https://www.cloudwhale.nl/launcher-files/AssetBundles/";

    public AssetBundle assetBundle;
    public bool finishedLoading = false;

    private bool _loadLocal;
    private float _totalProgress = 0;
    private float _maxProgress = 2f;
    private string _assetBundleName;

    public IEnumerator RetrieveAssetBundle(string assetBundleName, bool loadLocal = true)
    {
        finishedLoading = false;
        _assetBundleName = assetBundleName;
        _loadLocal = loadLocal;

        if (_loadLocal)
        {
            yield return LoadLocalBundle();
            if (!_loadLocal) yield return LoadRemoteBundle();

        }
        else
        {
            yield return LoadRemoteBundle();
            if (_loadLocal) yield return LoadLocalBundle();
        }
        finishedLoading = true;
    }

    IEnumerator LoadLocalBundle()
    {
        var localBundlePath = Path.Combine(Application.streamingAssetsPath, _assetBundleName);

        if (!File.Exists(localBundlePath))
        {
            _loadLocal = false;
            Debug.LogWarning("Unable to load asset bundle '" + _assetBundleName + "' from streaming assets! The asset file doesn't exist!");
            yield break;
        }

        AssetBundleCreateRequest localAssetBundleLoader;
        try
        {

            Debug.Log("Local bundle location: " + localBundlePath);
            localAssetBundleLoader = AssetBundle.LoadFromFileAsync(localBundlePath);
        }
        catch (Exception e)
        {
            _loadLocal = false;
            Debug.LogError("Unable to load asset bundle '" + _assetBundleName + "' from streaming assets! Reason: " + e);
            yield break;
        }

        yield return LocalProgressBroadcaster(localAssetBundleLoader);

        if (localAssetBundleLoader.assetBundle != null)
        {
            assetBundle = localAssetBundleLoader.assetBundle;
            _loadLocal = true;
        }
        else
        {
            _loadLocal = false;
            Debug.LogWarning("Unable to load asset bundle '" + _assetBundleName + "' from streaming assets!");
        }
    }

    IEnumerator LoadRemoteBundle()
    {
        var bundlePath = Path.Combine(baseUrl, _assetBundleName);

        Debug.Log("Remote bundle Location: " + bundlePath);

        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(bundlePath);
        
        yield return RemoteProgressBroadcaster(request);
        try
        {
            assetBundle = DownloadHandlerAssetBundle.GetContent(request);
            _loadLocal = false;
        }
        catch (Exception e)
        {
            _loadLocal = true;
            Debug.LogError("Unable to load asset bundle '" + _assetBundleName + "' from web! Reason: " + e);
        }
    }

    IEnumerator LocalProgressBroadcaster(AssetBundleCreateRequest request)
    {
        var localProgress = 0f;
        while (!request.isDone)
        {
            if (localProgress < request.progress)
            {
                localProgress = request.progress;
                OnProgress?.Invoke(_assetBundleName, localProgress + _totalProgress, _maxProgress);
            }
            yield return null;
        }
        _totalProgress += localProgress;
    }

    IEnumerator RemoteProgressBroadcaster(UnityWebRequest request)
    {
        var localProgress = 0f;
        var operation = request.SendWebRequest();
        while (!operation.isDone)
        {
            if (localProgress < operation.progress)
            {
                localProgress = operation.progress;
                OnProgress?.Invoke(_assetBundleName, localProgress + _totalProgress, _maxProgress);
            }
            yield return null;
        }
        _totalProgress += localProgress;
    }
}
