﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.UI;

public class IMGLoader
{
    public Texture2D texture;
    
    public static Sprite LoadNewSprite(string imgUrl, float pixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight)
    {
        // Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference
        Texture2D spriteTexture = LoadTextureFromDisk(imgUrl);
        return ConvertTextureToSprite(spriteTexture, pixelsPerUnit, spriteType);
    }

    public static Sprite ConvertTextureToSprite(Texture2D texture, float pixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight)
    {
        // Converts a Texture2D to a sprite, assign this texture to a new sprite and return its reference
        var newSprite = Sprite.Create(
            texture, 
            new Rect(0, 0, texture.width, texture.height), 
            new Vector2(0, 0), 
            pixelsPerUnit, 0, spriteType);
        return newSprite;
    }

    public static Texture2D LoadTextureFromDisk(string imgUrl)
    {
        // Load a PNG or JPG file from disk to a Texture2D
        // Returns null if load fails

        Texture2D tex2D;
        byte[] imgData;

        /*//Check if we should use UnityWebRequest or File.ReadAllBytes
        if (imgUrl.Contains("://") || imgUrl.Contains(":///"))
        {
            UnityWebRequest www = UnityWebRequest.Get(imgUrl);
            yield return www.SendWebRequest();
            imgData = www.downloadHandler.data;
            tex2D.LoadImage(imgData);
        }
        else*/ if (File.Exists(imgUrl))
        {
            imgData = File.ReadAllBytes(imgUrl);
            tex2D = new Texture2D(2, 2);           // Create new "empty" texture
            if (tex2D.LoadImage(imgData))           // Load the imagedata into the texture (size is set automatically)
                return tex2D;                 // If data = readable -> return texture
        }
        return null;                     // Return null if load failed
    }

    public IEnumerator LoadTexture(string imgUrl)
    {
        texture = new Texture2D(2,2);
        //Check if we should use UnityWebRequest or File.ReadAllBytes
        if (imgUrl.Contains("://") || imgUrl.Contains(":///"))
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(imgUrl);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            }
            DownloadHandlerTexture.GetContent(www);
            texture = DownloadHandlerTexture.GetContent(www);
        }
        else
        {
            var imgData = File.ReadAllBytes(imgUrl);
            texture.LoadImage(imgData);
        }
        yield break;
    }
}
